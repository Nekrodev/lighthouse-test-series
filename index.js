#!/usr/bin/env node
const fs = require("fs");
const lighthouse = require("lighthouse");
const chromeLauncher = require("chrome-launcher");

const url = process.argv[4]; // Change this to the url you want to run your performance tests against
const name = process.argv[3];
const runLimit = process.argv[2]; // Change this to be the number of performance tests you want to do
let runs = 0;

const results = [];

const auditsMapping = {
  "first-contentful-paint": "FCP",
  "largest-contentful-paint": "LCP",
  "first-meaningful-paint": "FMP",
  "speed-index": "SI",
  "total-blocking-time": "TBT",
  interactive: "TI",
  "server-response-time": "SRT",
};

const saveResult = (report, score) => {
  const audits = JSON.parse(report).audits;

  if (!audits) {
    return;
  }
  const result = { name, score };
  Object.keys(auditsMapping).forEach((key) => {
    result[auditsMapping[key]] = audits[key].numericValue;
  });
  results.push(result);
};

const runAudit = async () => {
  try {
    const chrome = await chromeLauncher.launch({
      chromeFlags: ["--headless"],
    });
    const options = {
      logLevel: "quiet",
      output: "json",
      onlyCategories: ["performance"],
      port: chrome.port,
    };
    const runnerResult = await lighthouse(url, options);
    saveResult(runnerResult.report, runnerResult.lhr.categories.performance.score * 100);

    await chrome.kill();
  } catch (err) {
    console.log(`Performance test ${runs + 1} failed`); // If Lighthouse happens to fail it'll log this to the console and log the error message
  }
};

(async () => {
  do {
    console.log(`Starting performance test ${runs + 1} on ${url}`); // Logs this to the console just before it kicks off
    await runAudit();
    runs++;
  } while (runs < runLimit); // Keeps looping around until this condition is false

  fs.writeFileSync(`${name}-report.json`, JSON.stringify(results));
  console.log('Done');
})();
