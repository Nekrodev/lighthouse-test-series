# README #

### What is this repository for? ###

* Run lighthouse test several times quietly in terminal

### How do I get set up? ###

* npm i

### How to run ###
* node index.js 10 test_name https://www.google.pl/
* 10 - amount of tests
* test_name - filename with report
* https://www.google.pl/ - tested url
### Who do I talk to? ###

* Tymofii Vdovichenko, timvd152@gmail.com